# HIFIS Teaser Video Script

This script details the scenes within the video shot by shot.


### General Agreements
* The video will be visuals only, without audio
* The outro and intro scenes should fit together to form an endless loop
* If any third party material is used, credits must be given to the creator
* All characters are stylized drawings representing a group of people
* Drawn assets need to have a Helmholtz-blue contour with light-green contrast outline and light blue fill so they stand out against screenshot elements
* The output format is 4k UHD (16:9) @ 50fps

### Characters
* **Sam** is a scientist (gender-neutral representation)
  * Required animation poses: 
    * [X] Stand 
    * [ ] reach to right
  * _Sam_ always stays in the foreground

### Drawn Assets

* [X] Thought bubble
* [ ] Doctor hat
* [X] Stack of documents
* [X] Laptop, frontal view
    * Bonus: Tablet
* [ ] Mouse pointer
    * Variant: Clicking
* [ ] Lightbulb
* [X] Server Cluster
* [ ] Formulae
* [X] Binary sequences
* [X] Scientific paper
  * With text "Thanks to HIFIS"
  * HIFIS Logo right in the center
* [X] Data Repository
* [ ] Credits text for Lorna in Handwriting (if not on Poster)

### Screenshot / Rendered Assets

## Shot Plan

| Shot № | Shot title     |Timestamp | Video Directions |
|--------|----------------|----------|------------------|
| 0      | intro          | 00:00.0  | Image [HIFIS logo](assets/HIFIS_Logo.png) + [Claim](assets/claim-scaled.png) is displayed                                              |
| 1      | project start  |          | _Sam_ moves into the screen from the left, until centered in the left third                                       |
|        |                |          | Thought-bubbles appear above _Sams_ head: Doctor hat, Documents titled "project"                                  |
|        |                |          | Laptop appears besides _Sam_ (to the right). The display says "Tools?"                                            |
|        |                |          | _Sam_ interacts (reach to right) with the laptop.                                                                 |
|        |                |          | [Animation](assets/type_hifis.net.mkv) appears, typing hifis.net                                                  |
|        |                |          | The laptop screen switches to [hifis.net](assets/hifis.net-1.png).                                                |
| 2      | hifis.net      |          | The laptop screen expands to become the background, keep only _Sam_ in the foreground                             |
|        |                |          | A Mouse pointer appears in the center of the screen                                                               |
|        |                |          | Mouse pointer moves to the "Helmholtz Cloud" button                                                               |
|        |                |          | Mouse pointer interacts (variant: clicking) with the "Helmholtz Cloud" button                                     |
| 3      | cloud services |          | Background changes to [cloud service screenshot](assets/cloud-portal-1.png), Mouse Pointer disappears, keep _Sam_ |
|        |                |          | Zoom ["GitLab" card](assets/cloud-portal-1-gitlabonly.png) to center                                              |
|        |                |          | Fade in [details section](assets/cloud-portal-2-gitlab-service-description.png) to the right                             |
|        |                |          | Thought bubble above _Sam_. Fade in content: "So many features." → "Help?" → "Training?"                          |
|        |                |          | Blend in or zoom to ["need help with..."](assets/cloud-portal-2-needhelp.png)                                     |
|        |                |          | Zoom to "Tutorials..." bullet points                                                                              |
| 4      | training       |          | Fade background to white. Remove thought bubble.                                                                  |
|        |                |          | Stack offers in from the right:                                                                                   |
|        |                |          | Tutorials: [title](assets/tutorials-title.png), [element 1](assets/tutorials-1.png), [element 2](assets/tutorials-2.png), [element 3](assets/tutorials-3.png), [element 4](assets/tutorials-4.png)   |
|        |                |          | [Courses](assets/courses.png)  |
|        |                |          | Consulting...?  |
|        |                |          | Add lightbulb above _Sams_ head                                                                                   |
|        |                |          | Stack out offers to the left, reveal Laptop with the Gitlab card on screen                                        |
| 5      | collaborating  |          | Zoom large on the ["GitLab" card](assets/cloud-portal-1-gitlabonly.png) to center, with Mouse pointer on its center     |
|        |                |          | Mouse pointer interacts (variant: clicking)                                                                       |
|        |                |          | Gitlab card gets Replaced by Helmholtz AAI [header](assets/aai-1.png) and (maybe stylized) [login section](assets/aai-2.png)   |
|        |                |          | Leave AAI header and replace AAI Login with [stylized local login](assets/idp-raw.png)                            |
|        |                |          | Username and Password get filled in (Center: "My Helmholtz Centre", [Username: "Sam Scientist", password is points](assets/idp-raw.png))                    |
|        |                |          | The login section fades out. A server cluster becomes visible behind it                                           |
|        |                |          | _Sams_ Laptop reappears                                                                                           |
|        |                |          | A connection line is drawn from the Laptop to the server cluster                                                  |
|        |                |          | Other smaller Laptops with smaller mirrored _Sams_ appear to the right                                            |
|        |                |          | Their Laptops also get connected to the server cluster                                                            |
|        |                |          | (Formulae and) binary sequences fade in and out around the server cluster                                         |
|        |                |          | Fade out server cluster, laptops and other _Sams_                                                                 |
| 6      | publication    |          | Fade in scientific paper at center of screen with data repository in the background                               |
|        |                |          | Text appears around: "Sustainable", "FAIR", "Collaborative" with arrows to the paper                              |
|        |                |          | Doctor Hat appears on _Sams_ head                                                                                 |
| 7      | outro          |          | _Sam_ (incl. hat) moves out to the left                                                                           |
|        |                |          | Fade out all except the HIFIS logo from the paper                                                                 |
|        |                |          | Zoom HIFIS logo to original position                                                                              |
|        |                |          | Claim appears                                                                                                     |
|        |                | 00:30.0  | (loop)                                                                                                            |

---

### Storyline

- Point 1 in lifecycle: Scientist Sam starts a new project and wonders which tools might be used for that
    - Question marks above head
    - *maybe:* dreams of doctor hat
- browses to [hifis.net](assets/hifis.net-1.png)
- browses to [cloud services](http://add-edulinks-to-gitlab-service.beermath-test.131.169.234.105.nip.io/services): [screenshot](assets/cloud-portal-1.png)
- zoom on gitlab card
    - click on [details](assets/cloud-portal-2.png)
- sweep through all the functionalities of gitlab
- more question marks on Sam: I can do soo much with that? But how?
- swap mor downwards to links on
    - [Tutorials](assets/tutorials.png)
    - Education / [Courses](assets/courses.png)
    - click & view
    - Lamp on Sam's head
- ~~Time passes by~~ ← Quite complex to convey without elaborate animation
- Sam is confident and knows what to do
- Clicks on on Gitlab at portal
- Helmholtz AAI „Use your own password/login from your centre“
    - *maybe* Here I can invite others and even non Helmholtz collaborators to my projects!
- Gitlab projekt with Gannt chart. << creat
    - the Gannt Chart displays the same 4 main steps of the lifecycle
    - ~~Birne am Sam (Erleuchtung)~~
    - Sam gets connected to the world and the collaborators (dotted lines)
- Steps 2-3 combined:
    - Sam clicks on cloud.helmholtz.de -> Openstack
    - Supercomputer
      - Supercomputer emits a bunch of complicated formulae
    - Zeroes and Ones appearing...
    - HIFIS support guy appears, speaking "sustainability, FAIR" ← What do you want to convey?
- Step 4:
    - Latex Paper in Gitlab
    - Data repository
    - thanks to the assistance by HIFIS: FAIR data, high impact publication
    - happy scientist -- medal appearing
- new rookie scientist appears (Sam 2), with as many question marks as Sam 1 had at the beginning
- Sam 1 talks to Sam 2 and points to HIFIS logo
- Fade out
- *maybe* some distortion, breaking up the peaceful fadeout
- Sam 1 re-appearing: "But I *still* have questions!"
    - 2-3 HIFIS guys appeaing with the 2 oversized badges (one with HIFIS logo)
    - speaking "Ask us!"
    - support@hifis.net
- All persons fade out
- End -> Start picture reappears: [HIFIS logo + Claim](assets/HIFIS_Logo_claim.svg)

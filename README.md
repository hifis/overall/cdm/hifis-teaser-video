# HIFIS Teaser Video

A teaser video to portray the services and tools offered by HIFIS

## Project structure

* **[script](script.md):** Verbal description of scenes
* **assets:** Static ressources like images, text fragments
* **scenes:** Singular cuts to be tied together.

## Rendered Videos
* [**Full Video**](https://gitlab.hzdr.de/hifis/overall/communication/hifis-teaser-video/-/jobs/artifacts/main/file/video/video_full.mp4?job=video_build)
* [Browse all rendered single videos](https://gitlab.hzdr.de/hifis/overall/communication/hifis-teaser-video/-/jobs/artifacts/main/browse/video/?job=video_build)
